var __sweetAlertAutoCloseTimer = 2000;

$(function () {
  setDefaultScroll();
  setDefaultFocus();

  if ($("#modalFilter")) {
    $("#modalFilter").on("shown.bs.modal", function () {
      $("#modalFilter .form-control")
        .not("[disabled]")
        .not("[readonly]")
        .first()
        .focus()
        .select();
    });
  }
}); // document loaded

function setDefaultScroll() {
  var e = $(".is-invalid.form-control")
    .not("[disabled]")
    .not("[readonly]")
    .first();
  if (e.length && e.parents("#modalFilter").length == 0) {
    var offset = e.offset();
    $("html, body").animate({ scrollTop: offset.top });
  } else {
    e = $(".form-control").not("[disabled]").not("[readonly]").first();
    if (e.length && e.parents("#modalFilter").length == 0) {
      var offset = e.offset();
      $("html, body").animate({ scrollTop: offset.top });
    }
  }
} // setDefaultScroll

function setDefaultFocus() {
  var e = $(".is-invalid.form-control")
    .not("[disabled]")
    .not("[readonly]")
    .first();
  if (e.length && e.parents("#modalFilter").length == 0) {
    e.focus().select();
  } else {
    e = $(".form-control").not("[disabled]").not("[readonly]").first();
    if (e.length && e.parents("#modalFilter").length == 0) {
      e.focus().select();
    }
  }
} // setDefaultFocus

function ajaxReturnJsonTrue(redirect) {
  Swal.fire({
    title: "Berhasil.",
    text: "Sebentar, sedang melanjutkan...",
    icon: "success",
    showConfirmButton: false,
    allowOutsideClick: false,
    allowEscapeKey: false,
  });
  if (redirect) {
    window.location.href = redirect;
  } else {
    location.reload();
  }
} // ajaxReturnJsonTrue

function ajaxReturnJsonFalse(message, dontReload) {
  Swal.fire({
    title: "Gagal.",
    icon: "error",
    text: message,
  }).then(function () {
    if (!dontReload) {
      ajaxShowLoading("Membaca ulang...");
      location.reload();
    }
  });
} // ajaxReturnJsonFalse

function ajaxReturnError(dontReload) {
  Swal.fire({
    title: "Error",
    text: "Proses Gagal.",
    icon: "error",
    showConfirmButton: false,
    timer: __sweetAlertAutoCloseTimer,
    timerProgressBar: true,
  }).then(function () {
    if (!dontReload) {
      ajaxShowLoading("Membaca ulang...");
      location.reload();
    }
  });
} // ajaxReturnError

function ajaxShowLoading(message) {
  if (!message || message == "") message = "Sedang memproses...";
  Swal.fire({
    text: message,
    showConfirmButton: false,
    allowOutsideClick: false,
    allowEscapeKey: false,
    didRender: () => {
      Swal.showLoading();
    },
  });
} // ajaxShowLoading

function sweetDeleteData(element) {
  if (!$("[name=_token]")) {
    Swal.fire("Laman tidak memiliki token.");
    return;
  }
  Swal.fire({
    text: "Yakin untuk menghapus data ini?",
    icon: "question",
    confirmButtonText: "Hapus",
    showCancelButton: true,
    cancelButtonText: "Batal",
    focusCancel: true,
  }).then((result) => {
    if (result.isConfirmed) {
      var url = $(element).data("deleteurl");
      var token = $("[name=_token]").val();
      ajaxShowLoading();
      $.ajax({
        type: "POST",
        url: url,
        data: { _token: token },
        success: function (resultData) {
          if (resultData.status) {
            ajaxReturnJsonTrue();
          } else {
            ajaxReturnJsonFalse(resultData.desc);
          }
        },
        error: function () {
          ajaxReturnError();
        },
      });
    }
  });
} // sweetDeleteData

function modalFormSetFocus(modalId, timeout = 0) {
  modalIdSelector = "#" + modalId;
  if (timeout) {
    setTimeout(() => {
      $(modalIdSelector + " .modal-body form input.form-control")
        .not("[disabled]")
        .not("[readonly]")
        .first()
        .focus()
        .select();
    }, timeout);
  } else {
    $(modalIdSelector + " .modal-body form input.form-control")
      .not("[disabled]")
      .not("[readonly]")
      .first()
      .focus()
      .select();
  }
} // modalFormSetFocus

function modalFormCreateHandler(modalId) {
  modalIdSelector = "#" + modalId;

  // #closeButton handler
  $(modalIdSelector + " .modal-body #closeButton").on("click", function () {
    bootstrap.Modal.getInstance(modalIdSelector).hide();
  });

  // #submitButton handler
  $(modalIdSelector + " .modal-body #submitButton").on("click", function () {
    form = $(modalIdSelector + " .modal-body form").first();

    // html5 browser validation
    if (!form[0].checkValidity()) {
      form[0].reportValidity();
      return false;
    }

    // captcha checking (if exist)
    captcha = form.find(".g-recaptcha");
    if (captcha.length) {
      response = grecaptcha.getResponse();
      if (response.length === 0) {
        Swal.fire({
          icon: "info",
          text: "Centang dulu capthcha: Saya bukan robot.",
        });
        return false;
      }
    }
    // form data preparation
    url = form.attr("action");
    data = form.serialize();
    contentType = form.data("ajax-content-type")
      ? form.data("ajax-content-type")
      : "application/x-www-form-urlencoded";
    processData = form.data("ajax-process-data") == "false" ? false : true;
    hasFile = form.find("input[type='file']").not("[disabled]").length;
    if (hasFile) {
      data = new FormData(form[0]);
      contentType = false;
      processData = false;
    }
    // clean up validation
    form
      .find(".form-control")
      .not("[disabled]")
      .not("[readonly]")
      .each(function () {
        $(this).removeClass("is-invalid");
        $(this).removeClass("is-valid");
      });
    form.find(".validation-error").remove();
    // form submission
    ajaxShowLoading();
    $.ajax({
      type: "POST",
      url: url,
      data: data,
      cache: false,
      contentType: contentType,
      processData: processData,
      success: function (resultData) {
        console.log(resultData);
        if (Swal.isVisible()) Swal.close();
        if (resultData.status) {
          ajaxReturnJsonTrue(resultData.redirect);
        } else {
          if (typeof resultData.validator !== "undefined") {
            form
              .find(".form-control")
              .not("[disabled]")
              .not("[readonly]")
              .each(function () {
                id = $(this).attr("id");
                if (resultData.validator[id]) {
                  $(this).addClass("is-invalid");
                  $(this).after(
                    '<small class="validation-error form-text d-block text-danger">' +
                      resultData.validator[id] +
                      "</small>"
                  );
                }
              });
          } else {
            form
              .find(".form-buttons")
              .before(
                '<div class="form-error validation-error alert alert-danger">' +
                  resultData.desc +
                  "</div>"
              );
          }
          form
            .find(".form-control")
            .not("[disabled]")
            .not("[readonly]")
            .not("is-invalid")
            .each(function () {
              $(this).addClass("is-valid");
            });
          modalFormSetFocus(modalId, 500);
        }
      },
      error: function () {
        if (Swal.isVisible()) Swal.close();
        ajaxReturnError();
      },
    });
  });
} // modalFormCreateHandler

function modalFormShow(e) {
  element = $(e);
  modalId = element.data("mfId");
  modalUrl = element.data("mfUrl");
  $("#" + modalId + " .modal-body").html(
    '<div id="spinner" class="text-center p-3"><div class="spinner-border spinner-border-lg" role="status"></div></div> <div id="formContainer" style="display: none;"></div>'
  );
  $("#" + modalId).modal("show");
  $.ajax({
    type: "GET",
    url: modalUrl,
    success: function (resultData) {
      $("#" + modalId + " .modal-body #formContainer").html(resultData);
      modalFormCreateHandler(modalId);
      $("#" + modalId + " .modal-body #spinner").hide();
      $("#" + modalId + " .modal-body #formContainer").show();
      modalFormSetFocus(modalId);
    },
    error: function () {
      ajaxReturnError();
    },
  });
}

function swalConfirm(e) {
  element = $(e);
  postUrl = element.data("postUrl");
  confirmMessage = element.data("confirmMessage");
  Swal.fire({
    text: confirmMessage,
    icon: "question",
    confirmButtonText: "Lanjutkan",
    showCancelButton: true,
    cancelButtonText: "Batalkan",
    focusCancel: true,
  }).then((result) => {
    if (result.isConfirmed) {
      token = $("[name=_token]").val();
      ajaxShowLoading();
      $.ajax({
        type: "POST",
        url: postUrl,
        data: { _token: token },
        success: function (resultData) {
          if (resultData.status) {
            ajaxReturnJsonTrue(resultData.redirect);
          } else {
            ajaxReturnJsonFalse(resultData.desc);
          }
        },
        error: function () {
          ajaxReturnError();
        },
      });
    }
  });
}
