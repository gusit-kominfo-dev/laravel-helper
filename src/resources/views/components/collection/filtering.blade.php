@php
    if ($isTersaring) {
        $btnFilterAttr = $attributes->merge(['class' => 'btn btn-filter btn-warning']);
    } else {
        $btnFilterAttr = $attributes->merge(['class' => 'btn btn-filter btn-outline btn-outline-primary']);
    }
    $modalBatalAttr = $attributes->merge(['class' => 'btn btn-outline btn-outline-secondary']);
    $modalResetAttr = $attributes->merge(['class' => 'btn btn-secondary']);
    $modalSaringAttr = $attributes->merge(['class' => 'btn btn-primary']);
@endphp

<a href="javascript:void(0);"
    data-bs-toggle="modal"
    data-bs-target="#modalFilter"
    title="{{ __('tooltip.btn_filter') }}"
    {{ $btnFilterAttr }}>
    <i class="fa-solid fa-filter"></i>
</a>

<x-collection-info :$collection :$isTersaring :$postUrl></x-collection-info>

<div class="modal fade" tabindex="-1" id="modalFilter" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Saring/Cari Data</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ $postUrl }}" method="GET" id="modalFilterForm">
                    {{ $slot }}
                    <div class="mt-5 pt-3 text-end">
                        <a href="javascript:void(0);" data-bs-dismiss="modal" {{ $modalBatalAttr }}>Batal</a>
                        <a href="{{ $postUrl }}" {{ $modalResetAttr }}>Reset</a>
                        <button type="submit" {{ $modalSaringAttr }}>Saring</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
