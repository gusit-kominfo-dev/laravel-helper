@php
    $attributes = $attributes->merge(['class' => 'btn btn-outline btn-outline-primary dropdown-toggle']);
@endphp

<span class="dropdown">
    <a href="javascript:void(0);" role="button" data-bs-toggle="dropdown" aria-expanded="false" {{ $attributes }}>
        {{ $selected }}
    </a>
    <ul class="dropdown-menu">
        @foreach ($options as $option)
            <li><a class="dropdown-item {{ $option == $selected ? 'active' : '' }}" href="{{ request()->fullUrlWithQuery(['rowsperpage' => $option]) }}">{{ $option }} baris</a></li>
        @endforeach
    </ul>
</span>
