<div class="mb-3">
    <label for="{{ $name }}" class="form-label">
        {{ $displayLabel }}
        @if ($required)
            <span class="text-danger">*</span>
        @endif
    </label>
    <input
        type="file"
        id="{{ $name }}"
        name="{{ $multiple ? $name . '[]' : $name }}"
        class="form-control @if (!$disabled) @error($name) is-invalid @else @if ($errors->any()) is-valid @endif @enderror @endif"
        @if ($required) required @endif
        @if ($disabled) disabled @endif
        @if ($accept) accept="{{ $accept }}" @endif
        @if ($multiple) multiple @endif
        {{ $attributes }} />
    @if (!$disabled)
        @error($name)
            <small class="validation-error form-text d-block text-danger">{{ $message }}</small>
        @enderror
    @endif
    @if ($displayHelpText)
        <small id='{{ $name . '.' . '_help' }}' class='form-text d-block text-muted'>{{ $displayHelpText }}</small>
    @endif
</div>
