<?php

namespace KominfoGusit\LaravelHelper\Rules;

use Illuminate\Contracts\Validation\Rule;
use KominfoGusit\LaravelHelper\AppHelper;

class TglBlnThn implements Rule
{
    public function passes($attribute, $value)
    {
        if (!preg_match("/[0-3][0-9]-[0-1][0-9]-[1|2][0|9][0-9][0-9]/", $value)) return false;
        $parsed = AppHelper::parseTanggal($value, 'j-n-Y');
        return $parsed;
    }
    public function message()
    {
        return 'validation.tglblnthn';
    }
}
