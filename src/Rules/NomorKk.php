<?php

namespace KominfoGusit\LaravelHelper\Rules;

use Illuminate\Contracts\Validation\Rule;

class NomorKk implements Rule
{
    public function passes($attribute, $value)
    {
        if (config('kominfohelper.validation_bypass_nokk')) {
            return true;
        } else {
            if (!preg_match("/^[0-9]{16}$/", $value)) return false;
            return true;
        }
    }

    public function message()
    {
        return 'validation.nomorkk';
    }
}
