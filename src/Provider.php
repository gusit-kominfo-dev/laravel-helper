<?php

namespace KominfoGusit\LaravelHelper;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

use KominfoGusit\LaravelHelper\Rules\Nik;
use KominfoGusit\LaravelHelper\Rules\NomorKk;
use KominfoGusit\LaravelHelper\Rules\TglBlnThn;
use KominfoGusit\LaravelHelper\View\Components\Collection\Filtering;
use KominfoGusit\LaravelHelper\View\Components\Collection\Info;
use KominfoGusit\LaravelHelper\View\Components\Collection\RowPerPage;
use KominfoGusit\LaravelHelper\View\Components\Form\File;
use KominfoGusit\LaravelHelper\View\Components\Form\Input;
use KominfoGusit\LaravelHelper\View\Components\Form\Recaptcha2;
use KominfoGusit\LaravelHelper\View\Components\Form\Select;
use KominfoGusit\LaravelHelper\View\Components\Form\Textarea;

class Provider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /* ---------------------------------- views --------------------------------- */

        $this->loadViewsFrom(__DIR__ . '/resources/views', 'kominfo-helper');

        /* ------------------------------- components ------------------------------- */

        Blade::component('form-input', Input::class);
        Blade::component('form-recaptcha2', Recaptcha2::class);
        Blade::component('form-file', File::class);
        Blade::component('form-select', Select::class);
        Blade::component('form-textarea', Textarea::class);

        Blade::component('collection-filtering', Filtering::class);
        Blade::component('collection-info', Info::class);
        Blade::component('collection-rowperpage', RowPerPage::class);

        /* -------------------------------- validator ------------------------------- */

        Validator::extend('tglblnthn', function ($attribute, $value, $parameters, $validator) {
            return (new TglBlnThn)->passes($attribute, $value);
        });
        Validator::extend('nik', function ($attribute, $value, $parameters, $validator) {
            return (new Nik)->passes($attribute, $value);
        });
        Validator::extend('nomorkk', function ($attribute, $value, $parameters, $validator) {
            return (new NomorKk)->passes($attribute, $value);
        });

        /* --------------------------------- publish -------------------------------- */

        $this->publishes([
            __DIR__ . '/config/kominfohelper.php' => config_path('kominfohelper.php'),
            __DIR__ . '/lang' => lang_path(),
            __DIR__ . '/public' => public_path(),
        ], 'kominfo-helper');
    }
}
