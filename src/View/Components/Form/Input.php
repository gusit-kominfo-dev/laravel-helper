<?php

namespace KominfoGusit\LaravelHelper\View\Components\Form;

use Illuminate\View\Component;

class Input extends Component
{
    public string $name;
    public string $label;
    public string $type;
    public string $langContext;
    public string $placeholder;
    public string $helpText;

    public bool $required;
    public bool $disabled;
    public bool $readOnly;

    public mixed $oldValue;

    public string $displayLabel;
    public string $displayHelpText;

    public function __construct($name, $langContext, $label = '', $type = "text", $placeholder = '', $helpText = '', $required = false, $disabled = false, $readOnly = false, $oldValue = null)
    {
        $this->name = $name;
        $this->label = $label;
        $this->type = $type;
        $this->langContext = $langContext;
        $this->placeholder = $placeholder;
        $this->helpText = $helpText;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->readOnly = $readOnly;
        $this->oldValue = $oldValue;

        $this->displayLabel = $this->label == '' ? __($langContext . "." . $name) : $this->label;
        if (!$helpText || $helpText == '') {
            $context = $langContext . "." . $name . "_help";
            $helpText = __($context);
            if ($helpText != $context) {
                $this->displayHelpText = __($langContext . "." . $name . "_help");
            } else {
                $this->displayHelpText = '';
            }
        } else {
            $this->displayHelpText = $helpText;
        }
    }

    public function render()
    {
        return view('kominfo-helper::components.form.input');
    }
}
