<?php

namespace KominfoGusit\LaravelHelper\View\Components\Form;

use Illuminate\View\Component;

class Recaptcha2 extends Component
{
    public string $name;
    public string $siteKey;
    public bool $modalForm;

    public function __construct($name, $siteKey, $modalForm = false)
    {
        $this->name = $name;
        $this->siteKey = $siteKey;
        $this->modalForm = $modalForm;
    }

    public function render()
    {
        return view('kominfo-helper::components.form.recaptcha2');
    }
}
